﻿using Model;
using SqlSugar;

namespace Repository
{
    public class RoleMenuRepository : BaseRepository<SysRoleMenu>, IDbAccess
    {
        public RoleMenuRepository(ISqlSugarClient db) : base(db)
        {
        }
    }
}