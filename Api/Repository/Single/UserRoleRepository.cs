﻿using Model;
using SqlSugar;

namespace Repository
{
    public class UserRoleRepository : BaseRepository<SysUserRole>, IDbAccess
    {
        public UserRoleRepository(ISqlSugarClient db) : base(db)
        {
        }
    }
}