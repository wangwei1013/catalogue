﻿using Model;
using SqlSugar;

namespace Repository
{
    public class CatalogRepository : BaseRepository<Catalog>, IDbAccess
    {
        public CatalogRepository(ISqlSugarClient db) : base(db)
        {
        }
    }
}