﻿using Model;
using SqlSugar;

namespace Repository
{
    public class TabRepository : BaseRepository<Tab>, IDbAccess
    {
        public TabRepository(ISqlSugarClient db) : base(db)
        {
        }
    }
}