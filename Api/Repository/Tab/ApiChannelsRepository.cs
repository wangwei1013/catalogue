﻿using Model;
using SqlSugar;

namespace Repository
{
    public class ApiChannelsRepository : BaseRepository<ApiChannels>, IDbAccess
    {
        public ApiChannelsRepository(ISqlSugarClient db) : base(db)
        {
        }
    }
}