﻿using Model;
using SqlSugar;

namespace Repository
{
    public class CatalogGroupingRepository : BaseRepository<CatalogGrouping>, IDbAccess
    {
        public CatalogGroupingRepository(ISqlSugarClient db) : base(db)
        {
        }
    }
}