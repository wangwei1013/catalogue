﻿using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;

namespace Application;

public static class AppServicesExtension
{
    public static IServiceCollection AddAppServices(this IServiceCollection services)
    {
        //注入services 层
        var appService = typeof(ApplicationService);
        var types = AppDomain.CurrentDomain.GetAssemblies()
            .SelectMany(s => s.GetTypes())
            .Where(p => (appService.IsAssignableFrom(p)) && p.IsClass && !p.IsAbstract && !p.IsInterface);
        foreach (var t in types)
        {
            services.AddTransient(t);
        }
        return services;
    }
}