﻿using Model;
using Model.ServiceModel;
using Newtonsoft.Json;

namespace Application;

public class LoginInputDTO
{
    public LoginInputDTO(string userAccount, string pwd)
    {
        UserAccount = userAccount;
        Pwd = pwd;
    }

    public string UserAccount { get; set; }
    public string Pwd { get; set; }
}

public class LoginOutputDTO
{
    public LoginUserInfo UserInfo { get; set; }
    public string Token { get; set; }
}

public class LoginUserInfo
{
    public int UserId { get; set; }
    public string UserName { get; set; }
    public string UserAccount { get; set; }

    [JsonIgnore]
    public IEnumerable<string> Roles { get; set; }

    [JsonIgnore]
    public List<TokenMenu> Menus { get; set; }

    public static LoginUserInfo FromSysUser(SysUser user)
    {
        var ui = new LoginUserInfo
        {
            UserId = user.Id,
            UserName = user.UserName,
            UserAccount = user.UserAccount,
            Roles = user.UserRoles.Select(p => p.RoleKey),
            Menus = new List<TokenMenu>()
        };
        foreach (var role in user.UserRoles)
        {
            foreach (var menu in role.Menus)
            {
                var roleMenu = role.RoleMenus.FirstOrDefault(p => p.MenuId == menu.Id);
                if (roleMenu == null) continue;
                ui.Menus.Add(new TokenMenu(menu.MenuKey, (MenuPermissionLevel)roleMenu.Level));
            }
        }
        return ui;
    }
}