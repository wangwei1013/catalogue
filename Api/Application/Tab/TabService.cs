﻿using Model;
using Repository;

namespace Application;

public class TabService : ApplicationService
{

    private readonly TabRepository _tabRepository;

    public TabService(TabRepository tabRepository)
    {
        _tabRepository = tabRepository;
    }

    public async Task<ApiReturn<Tab>> Get(int tabId)
    {
        Tab tab = await _tabRepository.GetByIdAsync(tabId);
        return RenderSuccess(tab);
    }

    public async Task<ApiReturn<string>> Add(Tab tab)
    {
        if (tab.TabId == 0) //新增
        {
            Tab tab1 = new Tab();
            tab1.Coding =  tab.Coding;
            tab1.Subject =  tab.Subject;
            tab1.EnglishName =  tab.EnglishName;
            tab1.TabPage =  tab.TabPage;
            tab1.CreateTime = DateTime.Now;
            await _tabRepository.InsertAsync(tab1);
            return RenderSuccess("成功");
        }
        else
        {
            Tab tab1 = await _tabRepository.GetByIdAsync(tab.TabId);
            tab1.Coding = tab.Coding;
            tab1.Subject = tab.Subject;
            tab1.EnglishName = tab.EnglishName;
            tab1.TabPage = tab.TabPage;
            tab1.UpdateTime = DateTime.Now;
            await _tabRepository.UpdateAsync(tab1);
            return RenderSuccess("成功");
        }
    }

    public async Task<ApiReturn<string>> Del(int tabId)
    {
        await _tabRepository.DeleteByIdAsync(tabId);
        return RenderSuccess("删除成功");
    }

}