﻿using Model;
using Repository;

namespace Application;

public class CatalogGroupingService : ApplicationService
{
    private readonly CatalogGroupingRepository _catalogGroupingRepository;

    public CatalogGroupingService(CatalogGroupingRepository catalogGroupingRepository)
    {
        _catalogGroupingRepository = catalogGroupingRepository;
    }

    public async Task<ApiReturn<CatalogGrouping>> Get(int id)
    {
        CatalogGrouping catalogGrouping = await _catalogGroupingRepository.GetByIdAsync(id);
        return RenderSuccess(catalogGrouping);
    }

    public async Task<ApiReturn<string>> Add(CatalogGrouping catalogGrouping)
    {
        if (catalogGrouping.Id == 0) //新增
        {
            CatalogGrouping catalogGrouping1 = new CatalogGrouping();
            catalogGrouping1.NodeId = catalogGrouping.NodeId;
            catalogGrouping1.ChannelUid = catalogGrouping.ChannelUid;
            catalogGrouping1.Sort = catalogGrouping.Sort;
            catalogGrouping1.CreateTime = DateTime.Now;
            await _catalogGroupingRepository.InsertAsync(catalogGrouping1);
            return RenderSuccess("成功");
        }
        else
        {
            CatalogGrouping catalogGrouping1 = await _catalogGroupingRepository.GetByIdAsync(catalogGrouping.Id);
            catalogGrouping1.NodeId = catalogGrouping.NodeId;
            catalogGrouping1.ChannelUid = catalogGrouping.ChannelUid;
            catalogGrouping1.Sort = catalogGrouping.Sort;
            catalogGrouping1.UpdateTime = DateTime.Now;
            await _catalogGroupingRepository.UpdateAsync(catalogGrouping1);
            return RenderSuccess("成功");
        }
    }

    public async Task<ApiReturn<string>> Del(int id)
    {
        await _catalogGroupingRepository.DeleteByIdAsync(id);
        return RenderSuccess("删除成功");
    }

}