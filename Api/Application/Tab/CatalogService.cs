﻿using Model;
using Repository;

namespace Application;

public class CatalogService : ApplicationService
{
    private readonly CatalogRepository _catalogRepository;

    public CatalogService(CatalogRepository catalogRepository)
    {
        _catalogRepository = catalogRepository;
    }

    public async Task<ApiReturn<Catalog>> Get(int nodeId)
    {
        Catalog catalog = await _catalogRepository.GetByIdAsync(nodeId);
        return RenderSuccess(catalog);
    }

    public async Task<ApiReturn<string>> Add(Catalog catalog)
    {
        if (catalog.node_id == 0) //新增
        {
            Catalog catalog1 = new Catalog();
            catalog1.Name = catalog.Name;
            catalog1.ParentId = catalog.ParentId;
            catalog1.Key = catalog.Key;
            catalog1.Level = catalog.Level;
            catalog1.Sort = catalog.Sort;
            catalog1.TypeCode = catalog.TypeCode;
            catalog1.CreateTime = DateTime.Now;
            await _catalogRepository.InsertAsync(catalog1);
            return RenderSuccess("成功");
        }
        else
        {
            Catalog catalog1 = await _catalogRepository.GetByIdAsync(catalog.node_id);
            catalog1.Name = catalog.Name;
            catalog1.ParentId = catalog.ParentId;
            catalog1.Key = catalog.Key;
            catalog1.Level = catalog.Level;
            catalog1.Sort = catalog.Sort;
            catalog1.TypeCode = catalog.TypeCode;
            catalog1.UpdateTime = DateTime.Now;
            await _catalogRepository.UpdateAsync(catalog1);
            return RenderSuccess("成功");
        }
    }

    public async Task<ApiReturn<string>> Del(int nodeId)
    {
        await _catalogRepository.DeleteByIdAsync(nodeId);
        return RenderSuccess("删除成功");
    }

}