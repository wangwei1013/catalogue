﻿using Model;
using Repository;

namespace Application;

public class ApiChannelsService : ApplicationService
{
    private readonly ApiChannelsRepository _apiChannelsRepository;

    public ApiChannelsService(ApiChannelsRepository apiChannelsRepository)
    {
        _apiChannelsRepository = apiChannelsRepository;
    }

    public async Task<ApiReturn<ApiChannels>> Get(int channelUid)
    {
        ApiChannels apiChannels = await _apiChannelsRepository.GetByIdAsync(channelUid);
        return RenderSuccess(apiChannels);
    }

    public async Task<ApiReturn<string>> Add(ApiChannels apiChannels)
    {
        if (apiChannels.ChannelUid == 0) //新增
        {
            ApiChannels apiChannels1 = new ApiChannels();
            apiChannels1.ChannelId = apiChannels.ChannelId;
            apiChannels1.DeviceId = apiChannels.DeviceId;
            apiChannels1.Name = apiChannels.Name;
            apiChannels1.Manufacturer = apiChannels.Manufacturer;
            apiChannels1.Model = apiChannels.Model;
            apiChannels1.Status = apiChannels.Status;
            apiChannels1.PtzType = apiChannels.PtzType;
            apiChannels1.Lng = apiChannels.Lng;
            apiChannels1.Lat = apiChannels.Lat;
            apiChannels1.LngGcj02 = apiChannels.LngGcj02;
            apiChannels1.LatGcj02 = apiChannels.LatGcj02;
            apiChannels1.LngWgs84 = apiChannels.LngWgs84;
            apiChannels1.LatWgs84 = apiChannels.LatWgs84;
            apiChannels1.CreatedAt = DateTime.Now;
            await _apiChannelsRepository.InsertAsync(apiChannels1);
            return RenderSuccess("成功");
        }
        else
        {
            ApiChannels apiChannels1 = await _apiChannelsRepository.GetByIdAsync(apiChannels.ChannelUid);
            apiChannels1.ChannelId = apiChannels.ChannelId;
            apiChannels1.DeviceId = apiChannels.DeviceId;
            apiChannels1.Name = apiChannels.Name;
            apiChannels1.Manufacturer = apiChannels.Manufacturer;
            apiChannels1.Model = apiChannels.Model;
            apiChannels1.Status = apiChannels.Status;
            apiChannels1.PtzType = apiChannels.PtzType;
            apiChannels1.Lng = apiChannels.Lng;
            apiChannels1.Lat = apiChannels.Lat;
            apiChannels1.LngGcj02 = apiChannels.LngGcj02;
            apiChannels1.LatGcj02 = apiChannels.LatGcj02;
            apiChannels1.LngWgs84 = apiChannels.LngWgs84;
            apiChannels1.LatWgs84 = apiChannels.LatWgs84;
            apiChannels1.UpdatedAt = DateTime.Now;
            await _apiChannelsRepository.UpdateAsync(apiChannels1);
            return RenderSuccess("成功");
        }
    }

    public async Task<ApiReturn<string>> Del(int channelUid)
    {
        await _apiChannelsRepository.DeleteByIdAsync(channelUid);
        return RenderSuccess("删除成功");
    }

}