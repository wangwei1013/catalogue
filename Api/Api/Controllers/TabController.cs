﻿using Application;
using Extension;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Model;

namespace Api.Controllers;

[Authorize]
public class TabController : BaseController
{
    private readonly TabService _tabService;

    public TabController(TabService tabService)
    {
        _tabService = tabService;
    }

    /// <summary>
    /// 查询
    /// </summary> 
    /// <returns></returns>
    [HttpPost("[action]")]
    [ProducesResponseType(typeof(ApiReturn<ApiChannels>), 200)]
    [AllowAnonymous]
    public async Task<IActionResult> Get(int id)
    {
        if (id == 0)
        {
            return RenderError("参数错误");
        }
        var apiReturn = await _tabService.Get(id);
        return new JsonResult(apiReturn);
    }

    /// <summary>
    /// 新增
    /// </summary> 
    /// <returns></returns>
    [HttpPost("[action]")]
    [ProducesResponseType(typeof(ApiReturn<string>), 200)]
    [AllowAnonymous]
    public async Task<IActionResult> Add(Tab tab)
    {
        var apiReturn = await _tabService.Add(tab);
        return new JsonResult(apiReturn);
    }

    /// <summary>
    /// 删除
    /// </summary> 
    /// <returns></returns>
    [HttpPost("[action]")]
    [ProducesResponseType(typeof(ApiReturn<string>), 200)]
    [AllowAnonymous]
    public async Task<IActionResult> Del(int id)
    {
        if (id == 0)
        {
            return RenderError("参数错误");
        }
        var apiReturn = await _tabService.Del(id);
        return new JsonResult(apiReturn);
    }


}