﻿using Application;
using Extension;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Model;

namespace Api.Controllers;

[Authorize]
public class CatalogGroupingController : BaseController
{
    private readonly CatalogGroupingService _catalogGroupingService;

    public CatalogGroupingController(CatalogGroupingService catalogGroupingService)
    {
        _catalogGroupingService = catalogGroupingService;
    }


    /// <summary>
    /// 查询
    /// </summary> 
    /// <returns></returns>
    [HttpPost("[action]")]
    [ProducesResponseType(typeof(ApiReturn<CatalogGrouping>), 200)]
    [AllowAnonymous]
    public async Task<IActionResult> Get(int id)
    {
        if (id == 0)
        {
            return RenderError("参数错误");
        }
        var apiReturn = await _catalogGroupingService.Get(id);
        return new JsonResult(apiReturn);
    }

    /// <summary>
    /// 新增
    /// </summary> 
    /// <returns></returns>
    [HttpPost("[action]")]
    [ProducesResponseType(typeof(ApiReturn<string>), 200)]
    [AllowAnonymous]
    public async Task<IActionResult> Add(CatalogGrouping catalogGrouping)
    {
        var apiReturn = await _catalogGroupingService.Add(catalogGrouping);
        return new JsonResult(apiReturn);
    }

    /// <summary>
    /// 删除
    /// </summary> 
    /// <returns></returns>
    [HttpPost("[action]")]
    [ProducesResponseType(typeof(ApiReturn<string>), 200)]
    [AllowAnonymous]
    public async Task<IActionResult> Del(int id)
    {
        if (id == 0)
        {
            return RenderError("参数错误");
        }
        var apiReturn = await _catalogGroupingService.Del(id);
        return new JsonResult(apiReturn);
    }

}