﻿using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Model;
using Model.ServiceModel;
using Newtonsoft.Json;

namespace Extension;

public class MenuAuthorizationMidd
{
    private readonly RequestDelegate _next;

    public MenuAuthorizationMidd(RequestDelegate next)
    {
        _next = next;
    }

    public async Task Invoke(HttpContext context)
    {
        var metaData = context.GetEndpoint()?.Metadata;
        if (metaData == null)
        {
            await _next.Invoke(context);
            return;
        }
        
        //如果没有菜单授权，则默认都可以访问
        if (!metaData.Any(p => p is MenuAuthorizationAttribute))
        {
            await _next.Invoke(context);
            return;
        }

        var menuAuthorizations = metaData.OfType<MenuAuthorizationAttribute>();
        var hasPermission = CheckPermission(context.User.Claims, menuAuthorizations);
        if (hasPermission == false)
        {
            var apiReturn = new ApiReturn(401, "当前用户无权限");
            await context.Response.WriteAsync(JsonConvert.SerializeObject(apiReturn));
            return;
        }
        await _next.Invoke(context);
    }

    private static bool CheckPermission(IEnumerable<Claim> claims, IEnumerable<MenuAuthorizationAttribute> menuAuthorizations)
    {
        if (claims.All(p => p.Type != "menus"))
        {
            return false;
        }
        //从登录的token中，获取用户的所具有的菜单权限
        var tokenMenus = claims.Where(p => p.Type == "menus").Select(p => JsonConvert.DeserializeObject<TokenMenu>(p.Value)).ToList();
        foreach (var menuAuthorization in menuAuthorizations)
        {
            if (string.IsNullOrEmpty(menuAuthorization.MenuKeys)) continue;
            var menuKeys = menuAuthorization.MenuKeys.Split(',');
            foreach (var menuKey in menuKeys)
            {
                var tokenMenu = tokenMenus.FirstOrDefault(p => p.MenuKey == menuKey);
                if (tokenMenu == null) continue;
                if (tokenMenu.Level >= menuAuthorization.Level)
                {
                    return true;
                }
            }
        }
        return false;
    }
}