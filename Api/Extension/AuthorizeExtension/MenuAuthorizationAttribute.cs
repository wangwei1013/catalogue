﻿using Model;
using Model.ServiceModel;

namespace Extension;

public class MenuAuthorizationAttribute : Attribute
{
    public MenuPermissionLevel Level { get; set; }
    public string MenuKeys { get; set; }

    public MenuAuthorizationAttribute(MenuPermissionLevel level, string menuKeys)
    {
        Level = level;
        MenuKeys = menuKeys;
    }
}