﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Model;

namespace Extension;

public static class ConfigExtension
{
    public static IServiceCollection AddConfig(this IServiceCollection services, ConfigurationManager config)
    {
        services.Configure<DbConfigModel>(config.GetSection(DbConfigModel.Position));
     //   services.Configure<EmailConfigModel>(config.GetSection(EmailConfigModel.Position));
        services.Configure<JwtConfigModel>(config.GetSection(JwtConfigModel.Position));
        return services;
    }
}