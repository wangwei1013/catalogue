﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Model.ServiceModel;
using Newtonsoft.Json;

namespace Extension;

public class JwtHelper
{
    private readonly JwtConfigModel _jwtConfig;

    public JwtHelper(IOptions<JwtConfigModel> jwtConfigOpts)
    {
        _jwtConfig = jwtConfigOpts.Value;
    }

    public string GenerateToken(string userName, IEnumerable<string> roles, IEnumerable<TokenMenu>? menus, int expireMinutes = 30)
    {
        // Configuring "Claims" to your JWT Token
        var claims = new List<Claim>
        {
            new(JwtRegisteredClaimNames.Sub, userName), // User.Identity.Name 
            new(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()), // JWT ID 
            new(ClaimTypes.Role, "user"),
        };

        claims.AddRange(roles.Select(p => new Claim(ClaimTypes.Role, p)));
        if (menus != null)
        {
            claims.AddRange(menus.Select(p => new Claim("menus", JsonConvert.SerializeObject(p))));
        }

        var userClaimsIdentity = new ClaimsIdentity(claims);

        // Create a SymmetricSecurityKey for JWT Token signatures
        var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtConfig.SignKey));
        var signingCredentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256Signature);
        var tokenDescriptor = new SecurityTokenDescriptor
        {
            Issuer = _jwtConfig.Issuer,
            //Audience = issuer, // Sometimes you don't have to define Audience.
            //NotBefore = DateTime.Now, // Default is DateTime.Now
            //IssuedAt = DateTime.Now, // Default is DateTime.Now
            Subject = userClaimsIdentity,
            Expires = DateTime.Now.AddMinutes(expireMinutes),
            SigningCredentials = signingCredentials
        };

        // Generate a JWT securityToken, than get the serialized Token result (string)
        var tokenHandler = new JwtSecurityTokenHandler();
        var securityToken = tokenHandler.CreateToken(tokenDescriptor);
        var serializeToken = tokenHandler.WriteToken(securityToken);

        return serializeToken;
    }
}