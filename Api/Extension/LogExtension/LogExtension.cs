﻿using System.Text;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using Model;

namespace Extension.LogExtension;

public static class LogExtension
{
    public static IServiceCollection AddCustomSerilog(this IServiceCollection services)
    {
        var root = AppDomain.CurrentDomain.BaseDirectory;
        var logPath = Path.Join(root, "logs/log-.txt");
        Log.Logger = new LoggerConfiguration()
            .MinimumLevel.Information()
            .WriteTo.Console()
            .WriteTo.File(logPath, rollingInterval: RollingInterval.Day)
            .CreateLogger();
        return services;
    }
}