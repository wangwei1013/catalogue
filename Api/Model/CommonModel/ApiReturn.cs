﻿namespace Model;

public class ApiReturn
{
    public int Code { get; set; }
    public string Message { get; set; }

    public bool Success => Code switch
    {
        >= 200 and < 300 => true,
        _                => false
    };

    public ApiReturn()
    {
    }

    public ApiReturn(int code, string message)
    {
        Code = code;
        Message = message;
    }
}

public class ApiReturn<T> : ApiReturn
{
    public T Data { get; set; }

    public ApiReturn()
    {
    }

    public ApiReturn(int code, string message, T data)
    {
        Code = code;
        Message = message;
        Data = data;
    }
}

public class ApiReturnPage<T> : ApiReturn<T>
{
    public new List<T> Data { get; set; }
    public int Total { get; set; }

    public ApiReturnPage(List<T> data, int total)
    {
        this.Data = data;
        Total = total;
    }
}