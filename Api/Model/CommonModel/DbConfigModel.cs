﻿using SqlSugar;

namespace Model;

public class DbConfigModel
{
    public static string Position = "DbConfig";
    public string ConnectionString { get; set; }
    public DbType DbType { get; set; } = DbType.MySql;
}