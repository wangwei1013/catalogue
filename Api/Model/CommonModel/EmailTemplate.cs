﻿public static class EmailTemplate
{
    private const string HELLO_WORLD = @"hello world {0}";

    public static string GetTemplateContent(EmailTemplateType type, params string[] args)
    {
        var template = type switch
        {
            EmailTemplateType.HelloWorld => HELLO_WORLD,
            _                            => HELLO_WORLD
        };
        return string.Format(template, args);
    }
}

public enum EmailTemplateType
{
    HelloWorld,
}