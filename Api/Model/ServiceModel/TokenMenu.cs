﻿using Newtonsoft.Json;

namespace Model.ServiceModel;

public class TokenMenu
{
    [JsonProperty("k")]
    public string MenuKey { get; set; }

    [JsonProperty("l")]
    public MenuPermissionLevel Level { get; set; }

    public TokenMenu(string menuKey, MenuPermissionLevel level)
    {
        this.MenuKey = menuKey;
        Level = level;
    }
}