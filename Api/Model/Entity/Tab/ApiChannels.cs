﻿using SqlSugar;
namespace Model;

[SugarTable("api_channels")]
public class ApiChannels
{
    [SugarColumn(ColumnName = "channel_uid", IsPrimaryKey = true, IsIdentity = true)]
    public int ChannelUid { get; set; }

    [SugarColumn(ColumnName = "channel_id")]
    public string ChannelId { get; set; }

    [SugarColumn(ColumnName = "device_id")]
    public string DeviceId { get; set; }

    [SugarColumn(ColumnName = "name")]
    public string Name { get; set; }

    [SugarColumn(ColumnName = "manufacturer")]
    public string Manufacturer { get; set; }

    [SugarColumn(ColumnName = "model")]
    public string Model { get; set; }

    [SugarColumn(ColumnName = "status")]
    public string Status { get; set; }

    [SugarColumn(ColumnName = "ptz_type")]
    public int PtzType { get; set; }

    [SugarColumn(ColumnName = "lng")]
    public double Lng { get; set; }

    [SugarColumn(ColumnName = "lat")]
    public double Lat { get; set; }

    [SugarColumn(ColumnName = "lng_gcj02")]
    public double LngGcj02 { get; set; }

    [SugarColumn(ColumnName = "lat_gcj02")]
    public double LatGcj02 { get; set; }

    [SugarColumn(ColumnName = "lng_wgs84")]
    public double LngWgs84 { get; set; }

    [SugarColumn(ColumnName = "lat_wgs84")]
    public double LatWgs84 { get; set; }

    [SugarColumn(ColumnName = "created_at")]
    public DateTime CreatedAt { get; set; }

    [SugarColumn(ColumnName = "updated_at")]
    public DateTime UpdatedAt { get; set; }
}