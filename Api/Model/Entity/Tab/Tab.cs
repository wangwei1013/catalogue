﻿using SqlSugar;

namespace Model;

[SugarTable("tab")]
public class Tab
{
    [SugarColumn(ColumnName = "tab_id", IsPrimaryKey = true)]
    public int TabId { get; set; }

    [SugarColumn(ColumnName = "coding")]
    public string Coding { get; set; }

    [SugarColumn(ColumnName = "subject")]
    public string Subject { get; set; }

    [SugarColumn(ColumnName = "english_name")]
    public string EnglishName { get; set; } 

    [SugarColumn(ColumnName = "tab_page")]
    public string TabPage { get; set; }

    [SugarColumn(ColumnName = "create_time")]
    public DateTime CreateTime { get; set; }

    [SugarColumn(ColumnName = "update_time")]
    public DateTime UpdateTime { get; set; }

}