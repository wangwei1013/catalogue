﻿using SqlSugar;

namespace Model;

[SugarTable("catalog")]
public class Catalog
{

    [SugarColumn(ColumnName = "node_id", IsPrimaryKey = true)]
    public int node_id { get; set; }

    [SugarColumn(ColumnName = "name")]
    public string Name { get; set; }
    
    [SugarColumn(ColumnName = "parent_id")]
    public string ParentId { get; set; }

    [SugarColumn(ColumnName = "key")]
    public string Key { get; set; }

    [SugarColumn(ColumnName = "level")]
    public string Level { get; set; }   
    
    [SugarColumn(ColumnName = "sort")]
    public int Sort { get; set; } 
    
    [SugarColumn(ColumnName = "type_code")]
    public string TypeCode { get; set; }

    [SugarColumn(ColumnName = "create_time")]
    public DateTime CreateTime { get; set; }

    [SugarColumn(ColumnName = "update_time")]
    public DateTime UpdateTime { set; get; }
}