﻿using SqlSugar;
namespace Model;

[SugarTable("catalog_grouping")]
public class CatalogGrouping
{
    [SugarColumn(ColumnName = "id", IsPrimaryKey = true)]
    public int Id { get; set; }

    [SugarColumn(ColumnName = "node_id")]
    public int NodeId { get; set; }

    [SugarColumn(ColumnName = "channel_uid")]
    public string ChannelUid { get; set; } 

    [SugarColumn(ColumnName = "sort")]
    public int Sort { get; set; }

    [SugarColumn(ColumnName = "create_time")]
    public DateTime CreateTime { get; set; }

    [SugarColumn(ColumnName = "update_time")]
    public DateTime UpdateTime { set; get; }
}