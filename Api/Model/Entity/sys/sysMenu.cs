﻿using SqlSugar;
using System.ComponentModel;

namespace Model;

[SugarTable("sysMenu")]
public class SysMenu : IdEntity
{
    /// <summary>
    /// 菜单名称
    /// </summary>
    [SugarColumn()]
    public string MenuName { get; set; }

    /// <summary>
    /// 菜单父id
    /// </summary>
    [SugarColumn()]
    public int MenuParentId { get; set; }

    /// <summary>
    /// 菜单的唯一key
    /// </summary>
    [SugarColumn(IsOnlyIgnoreUpdate = true)]
    public string MenuKey { get; set; }

    /// <summary>
    /// 菜单路径
    /// </summary>
    [SugarColumn(IsOnlyIgnoreUpdate = true)]
    public string MenuUrl { get; set; }

    /// <summary>
    /// 菜单图标
    /// </summary>
    [SugarColumn(IsOnlyIgnoreUpdate = true)]
    public string MenuIcon { get; set; }

    /// <summary>
    /// 排序
    /// </summary>
    [SugarColumn(DefaultValue = "0")]
    public int MenuSort { get; set; }

    /// <summary>
    /// 是否启用，1是 2否
    /// </summary>
    [SugarColumn(DefaultValue = "1")]
    public int MenuEnable { get; set; }


    //菜单管理启用状况
    public enum MenuStatusEnum
    {
        /// <summary>
        /// 设备配置状态开启
        /// </summary>
        [Description("开启")]
        Enable = 1,

        /// <summary>  
        /// 设备配置状态状禁用
        /// </summary>
        [Description("禁用")]
        Disable = 2,
    }
}

public class MenuKeys
{
    public const string UserManagement = "user";  
    public const string OrderManagement = "order";
    public const string GoodsManagement = "goods";
}