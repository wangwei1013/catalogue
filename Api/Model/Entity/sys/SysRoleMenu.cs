﻿using System;
using SqlSugar;

namespace Model;

[SugarTable("sysRoleMenu")]
public class SysRoleMenu : Entity
{
    /// <summary>
    /// 角色Id
    /// </summary>
    [SugarColumn(IsPrimaryKey = true)]
    public int RoleId { get; set; }

    /// <summary>
    /// 菜单Id
    /// </summary>
    [SugarColumn(IsPrimaryKey = true)]
    public int MenuId { get; set; }

    /// <summary>
    /// 权限等级, 0 可读，1可写，2可删
    /// </summary>
    [SugarColumn()]
    public int Level { get; set; }

    /// 创建时间
    /// </summary>
    [SugarColumn(IsOnlyIgnoreUpdate = true)]
    public DateTime CreateTime { get; set; }

    /// <summary>
    /// 创建人
    /// </summary>
    [SugarColumn(IsOnlyIgnoreUpdate = true)]
    public int CreateUserId { set; get; }
}

public enum MenuPermissionLevel
{
    Read,
    Write,
    Delete
}