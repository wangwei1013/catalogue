﻿using System;
using SqlSugar;

namespace Model;

[SugarTable("sysRole")]
public class SysRole : IdEntity
{
    /// <summary>
    /// 角色Key
    /// </summary>
    [SugarColumn()]
    public string RoleKey { get; set; }

    /// <summary>
    /// 角色名称
    /// </summary>
    [SugarColumn()]
    public string RoleName { get; set; }
    
    /// <summary>
    /// 备注
    /// </summary>
    [SugarColumn()]
    public string Comment { get; set; }


    [Navigate(typeof(SysUserRole), nameof(SysUserRole.RoleId), nameof(SysUserRole.UserId))]
    public List<SysUser> Users { get; set; }

    [Navigate(typeof(SysRoleMenu), nameof(SysRoleMenu.RoleId), nameof(SysRoleMenu.MenuId))]
    public List<SysMenu> Menus { get; set; }

    [Navigate(NavigateType.OneToMany, nameof(SysRoleMenu.RoleId))]
    public List<SysRoleMenu> RoleMenus { get; set; }

    /// <summary>
    /// 创建时间
    /// </summary>
    [SugarColumn(IsOnlyIgnoreUpdate = true)]
    public DateTime CreateTime { get; set; }

    /// <summary>
    /// 创建人
    /// </summary>
    [SugarColumn(IsOnlyIgnoreUpdate = true)]
    public int CreateUserId { set; get; }
}

public class RoleKeys
{
    /// <summary>
    /// 管理员，具有所有权限
    /// </summary>
    public const string Admin = "admin";

    /// <summary>
    /// 销售员，能看订单和商品
    /// </summary>
    public const string Salesman = "salesman";

    /// <summary>
    /// 仓管员，只能看商品和库存
    /// </summary>
    public const string Storeman = "storeman";
}