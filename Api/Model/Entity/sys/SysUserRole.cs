﻿using System;
using SqlSugar;

namespace Model;

[SugarTable("sysUserRole")]
public class SysUserRole: Entity
{
    /// <summary>
    /// 角色Id
    /// </summary>
    [SugarColumn(IsPrimaryKey = true)]
    public int RoleId { get; set; }

    /// <summary>
    /// 用户Id
    /// </summary>
    [SugarColumn(IsPrimaryKey = true)]
    public int UserId { get; set; }

    /// <summary>
    /// 创建时间
    /// </summary>
    [SugarColumn(IsOnlyIgnoreUpdate = true)]
    public DateTime CreateTime { get; set; }

    /// <summary>
    /// 创建人
    /// </summary>
    [SugarColumn(IsOnlyIgnoreUpdate = true)]
    public int CreateUserId { set; get; }
}